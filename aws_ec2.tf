AWSTemplateFormatVersion: 2010-09-09
Resources:
  ExampleSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref ExampleVpc
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22 # SSH traffic
          CidrIp: "0.0.0.0/0" # from all IP addresses is authorized
